package filter;
import java.io.IOException;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
public class TestFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("sono il filter!");
		
		if (req.getParameter("nome") != null) {
			chain.doFilter(req, resp);
		}else{
			HttpServletResponse response = (HttpServletResponse) resp;
	        response.sendRedirect("Error.jsp");
		}
	}

}
