package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloServlet extends HttpServlet {
	private static final String HELLO_WORLD_SERVLET = "<HTML><HEAD><TITLE>Ciao Mondo</TITLE></HEAD><BODY><H1> CIAO %NOME%</H1></BODY></HTML>";
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException{
		try {
		    String tizio = req.getParameter("nome")==null?"nessuno":req.getParameter("nome");
		    String out = HELLO_WORLD_SERVLET.replaceAll("%NOME%", tizio);
		    resp.getOutputStream().write(out.getBytes());
		} catch (IOException e) {
		}
	    
	}
}
