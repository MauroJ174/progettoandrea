package it.dstech.it.mauro;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ALUNNI", schema = "UNIVERSITA")
public class Alunni {

	private String nome;
	private String cognome;
	@Id
	@GeneratedValue
	@Column(name = "ALUNNI_ID")
	private int id;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
	@JoinTable(name = "Relazione", schema = "UNIVERSITA", joinColumns = {
			@JoinColumn(name = "ALUNNI_ID") }, inverseJoinColumns = { @JoinColumn(name = "CORSO_ID") })
	private List<Corsi> corsi = new ArrayList<>();

	public List<Corsi> getCorsi() {
		return corsi;
	}

	public void setCorsi(List<Corsi> corsi) {
		this.corsi = corsi;
	}

	public Alunni() {
		super();
		this.nome = nome;
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
