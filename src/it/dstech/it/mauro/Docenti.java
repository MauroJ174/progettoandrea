package it.dstech.it.mauro;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "DOCENTI", schema = "UNIVERSITA")
public class Docenti {
	String nome;
	String cognome;
	@Id
	@GeneratedValue
	@Column(name = "PROF_ID")
	int idDocente;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "docente", cascade = CascadeType.REMOVE)
	private List<Corsi> listCorsi = new ArrayList<>();

	public Docenti() {
		super();
		this.nome = nome;
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getIdDocente() {
		return idDocente;
	}

	public void setIdDocente(int idDocente) {
		this.idDocente = idDocente;
	}

}
