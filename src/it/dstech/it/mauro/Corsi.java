package it.dstech.it.mauro;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CORSI", schema = "UNIVERSITA")
public class Corsi {
	String nome;

	@Id
	@GeneratedValue
	@Column(name = "CORSI_ID")
	int idCorso;
	public Docenti getDocente() {
		return docente;
	}

	public void setDocente(Docenti docente) {
		this.docente = docente;
	}

	public List<Alunni> getAlunni() {
		return alunni;
	}

	public void setAlunni(List<Alunni> alunni) {
		this.alunni = alunni;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PROF_ID")
	private Docenti docente;

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "corsi")
	private List<Alunni> alunni = new ArrayList<Alunni>();

	public Corsi() {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdCorso() {
		return idCorso;
	}

	public void setIdCorso(int idCorso) {
		this.idCorso = idCorso;
	}

}
